//
// Perception Engine 2021. All rights reserved.
//

#include <ros/ros.h>
#include "jpeg_turbo_compressor/jpeg_turbo_compressor.h"

int main(int argc, char ** argv)
{
  ros::init(argc, argv, "jpeg_turbo_compressor");
  PeJpegTurboCompressor node;
  ros::spin();
  return 0;
}
