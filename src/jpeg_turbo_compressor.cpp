//
// Perception Engine 2021. All rights reserved.
//
#include <jpeg_turbo_compressor/jpeg_turbo_compressor.h>

PeJpegTurboCompressor::PeJpegTurboCompressor() : nh_(""), private_nh_("~"), compression_rate_(60.)
{
  private_nh_.param<int>("compression_rate", compression_rate_, 60);

  compression_rate_ = std::max(100, compression_rate_);
  compression_rate_ = std::max(0, compression_rate_);

  ROS_INFO_STREAM("Compression Rate:" << compression_rate_);

  jpeg_compressor_ = tjInitCompress();
  ROS_INFO_STREAM("Turbo Jpeg Initialized correctly");

  std::string input_topic;
  private_nh_.param<std::string>("input", input_topic, "image_raw");

  private_nh_.param<bool>("show_compression_time", show_compression_time_, false);

  ROS_INFO_STREAM("Subscribing to " << input_topic);
  sub_ = nh_.subscribe(input_topic, 1, &PeJpegTurboCompressor::ImageCallback, this);
  pub_ = nh_.advertise<sensor_msgs::CompressedImage>(input_topic+"/compressed", 1, false);
  ROS_INFO_STREAM("Publishing to " << input_topic << "/compressed");
}

PeJpegTurboCompressor::~PeJpegTurboCompressor()
{
  if(jpeg_compressor_)
    tjDestroy(jpeg_compressor_);

}

void PeJpegTurboCompressor::ImageCallback(
  const sensor_msgs::Image ::ConstPtr & input_msg)
{
  long unsigned int jpegSize = 0;
  unsigned char* compressedImage = NULL;
  const unsigned char* input_image = input_msg->data.data();

  int jpeg_subsample = 0;
  bool compressYuv = false;

  std::string encoding = input_msg->encoding;
  cv::Mat mat;
  if (input_msg->encoding == sensor_msgs::image_encodings::BAYER_RGGB8
      || input_msg->encoding == sensor_msgs::image_encodings::BAYER_BGGR8
      || input_msg->encoding == sensor_msgs::image_encodings::BAYER_GBRG8
      || input_msg->encoding == sensor_msgs::image_encodings::BAYER_GRBG8
    )
  {
    cv_bridge::CvImagePtr cv_ptr;
    try
    {
      cv_ptr = cv_bridge::toCvCopy(input_msg, sensor_msgs::image_encodings::BGR8);
      encoding = sensor_msgs::image_encodings::BGR8;
      mat = cv_ptr->image;
      input_image = mat.datastart;
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }
  }

  if (encoding == sensor_msgs::image_encodings::RGB8)
  {
    jpeg_subsample = TJPF_RGB;
  }
  else if (encoding == sensor_msgs::image_encodings::BGR8)
  {
    jpeg_subsample = TJPF_BGR;
  }
  else if(encoding == sensor_msgs::image_encodings::MONO8)
  {
    jpeg_subsample = TJPF_GRAY;
  }
  else if(encoding == sensor_msgs::image_encodings::YUV422)
  {
    compressYuv = true;
  }
  else
  {
    ROS_ERROR_STREAM("Unknown Input Image type." << input_msg->encoding <<"  Not converting.");
    return;
  }

  if(!compressYuv)
  {
    auto start = std::chrono::high_resolution_clock::now();
    tjCompress2(jpeg_compressor_,
                input_image,
                input_msg->width,
                0,
                input_msg->height,
                jpeg_subsample,
                &compressedImage,
                &jpegSize,
                TJSAMP_420,
                compression_rate_,
                TJFLAG_FASTDCT);
    auto elapsed = std::chrono::high_resolution_clock::now() - start;

    if (show_compression_time_)
    {
      ROS_INFO_STREAM("Compression time:" <<std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count()
                        << " ms");

    }
    sensor_msgs::CompressedImagePtr out_img(new sensor_msgs::CompressedImage());
    out_img->header = input_msg->header;
    out_img->data.resize(jpegSize);
    out_img->format="jpeg";
    std::copy(compressedImage,
              compressedImage + jpegSize,
              out_img->data.begin());

    tjFree(compressedImage);
    pub_.publish(out_img);
  }
  else
  {
    auto start = std::chrono::high_resolution_clock::now();
    const int PADDING = 2;
    if (tjCompressFromYUV(jpeg_compressor_, 
                       input_image, 
                       input_msg->width, 
                       PADDING, 
                       input_msg->height,
                       TJSAMP_422,
                       &compressedImage,
                       &jpegSize,
                       compression_rate_,
                       TJFLAG_FASTDCT ))
    {
      std::cout << "ERROR: " << tjGetErrorStr() << std::endl;
      return;
    }
    auto elapsed = std::chrono::high_resolution_clock::now() - start;

    if (show_compression_time_)
    {
      ROS_INFO_STREAM("Compression time:" <<std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count()
                        << " ms");

    }
    sensor_msgs::CompressedImagePtr out_img(new sensor_msgs::CompressedImage());
    out_img->header = input_msg->header;
    out_img->data.resize(jpegSize);
    out_img->format="jpeg";
    std::copy(compressedImage,
              compressedImage + jpegSize,
              out_img->data.begin());

    tjFree(compressedImage);
    pub_.publish(out_img);
  }

}
