//
// Perception Engine 2021. All rights reserved.
//

#ifndef PE_IMAGE_COMPRESSOR_JPEG_TURBO_COMPRESSOR_H
#define PE_IMAGE_COMPRESSOR_JPEG_TURBO_COMPRESSOR_H

#include <string>
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CompressedImage.h>
#include <sensor_msgs/image_encodings.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <chrono>

#include <turbojpeg.h>

class PeJpegTurboCompressor
{
private:  // ros
  ros::NodeHandle nh_;
  ros::NodeHandle private_nh_;
  ros::Publisher pub_;
  ros::Subscriber sub_;

  int compression_rate_;
  bool show_compression_time_;
  void ImageCallback(
    const sensor_msgs::Image ::ConstPtr & input_msg);

  tjhandle jpeg_compressor_;//turbojpeg compressor

public:
  PeJpegTurboCompressor();
  ~PeJpegTurboCompressor();
};


#endif //PE_IMAGE_COMPRESSOR_JPEG_TURBO_COMPRESSOR_H
