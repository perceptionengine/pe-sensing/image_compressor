# Image compressor

## TurboJpeg

### Requisites

`apt-get install -y libturbojpeg0-dev`

### Compile

`catkin build --cmake-args -DCMAKE_BUILD_TYPE=Release`

### Launch

`roslaunch pe_image_compressor turbo_jpeg.launch compression_rate:=60 input:=/camera/image_raw`

---

## NvJpeg

